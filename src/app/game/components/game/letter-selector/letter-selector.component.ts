import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {SelectableLetter} from '../../../models/selectable-letter.model';

@Component({
  selector: 'app-letter-selector',
  templateUrl: './letter-selector.component.html',
  styleUrls: ['./letter-selector.component.scss']
})
export class LetterSelectorComponent {
  @Input() letterList: Array<SelectableLetter>;
  @Input() finished: boolean;
  @Input() abc: string;
  @Output() selectedLetter = new EventEmitter<string>();

  onLetter(letter: SelectableLetter): void {
    letter.tried = true;
    this.selectedLetter.emit(letter.letter);
  }

  @HostListener('window:keydown', ['$event'])
  keydown(event: KeyboardEvent): void {
    const index = this.abc.indexOf(event.key.toLowerCase());
    if (index > -1 && !this.letterList[index].tried) {
      this.onLetter(this.letterList[index]);
    }
  }

}
