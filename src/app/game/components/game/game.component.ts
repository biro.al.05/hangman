import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {HangmanService} from '../../../shared/hangman.service';
import {WordLetter} from '../../models/word-letter.model';
import {SelectableLetter} from '../../models/selectable-letter.model';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {

  subscriptions = new Subscription();
  wordLetters: Array<WordLetter> = [];
  selectableLetters: Array<SelectableLetter> = [];
  abc = 'abcdefghijklmnopqrstuvwxyz';

  score = 0;
  maxScore = 10;
  lettersFoundCount = 0;
  gameOver: 'WIN' | 'LOSE' = null;

  constructor(private hangmanService: HangmanService) { }

  ngOnInit(): void {
    this.initLetters();
    this.initWord();
  }

  initLetters(): void {
    for (const char of this.abc) {
      this.selectableLetters.push(new SelectableLetter(char));
    }
  }


  initWord(): void {
    this.wordLetters = [];
    this.lettersFoundCount = 0;
    this.score = 0;

    const lastWord = localStorage.getItem('wordLetters');
    if (lastWord) {
      lastWord.split('|').forEach(char => {
        if (char[0] === '_') {
          this.wordLetters.push(new WordLetter(char[1]));
        } else {
          this.wordLetters.push(new WordLetter(char, true));
          this.lettersFoundCount++;
        }
      });
      const lastTips = localStorage.getItem('selectableLetters');
      lastTips.split('|').forEach((letterSelected, i) => {
        this.selectableLetters[i].tried = letterSelected === 'true';
      });
      this.score = +localStorage.getItem('score');
    } else {
      const word = this.hangmanService.getRandomWord();
      for (const char of word) {
        this.wordLetters.push(new WordLetter(char));
      }
      this.saveToLocalStorage();
    }
  }

  onLetter(letter: string): void {
    let hit = false;
    this.wordLetters.forEach(wletter => {
      if (wletter.letter.toLowerCase() === letter) {
        wletter.found = true;
        this.lettersFoundCount++;
        hit = true;
      }
    });
    if (!hit) {
      this.score++;
    }

    this.saveToLocalStorage();
    this.checkWin();
  }

  saveToLocalStorage(): void {
    localStorage.setItem('wordLetters', this.wordLetters.map(l => l.found ? l.letter : `_${l.letter}`).join('|'));
    localStorage.setItem('selectableLetters', this.selectableLetters.map(l => l.tried).join('|'));
    localStorage.setItem('score', this.score.toString());
  }

  checkWin(): void {
    if (this.score === this.maxScore) {
      this.gameOver = 'LOSE';
    } else if (this.lettersFoundCount === this.wordLetters.length) {
      this.gameOver = 'WIN';
    }
    if (this.gameOver) {
      localStorage.clear();
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }


}
