import {Component, OnDestroy, OnInit} from '@angular/core';
import {HangmanService} from '../../../shared/hangman.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-game-settings',
  templateUrl: './game-settings.component.html',
  styleUrls: ['./game-settings.component.scss']
})
export class GameSettingsComponent implements OnInit, OnDestroy {

  subscriptions = new Subscription();
  lengths: Array<number> = [];

  selectedLenght = -1;

  constructor(private hangmanService: HangmanService) { }

  ngOnInit(): void {
    localStorage.clear();
    this.getWordLength();
  }

  getWordLength(): void {
    this.subscriptions.add(this.hangmanService.wordLengths.subscribe(lengths => {
      this.lengths = lengths;
    }));
  }

  selectLength(lenght: number): void {
    this.selectedLenght = lenght;
    this.hangmanService.selectedLength = lenght;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
