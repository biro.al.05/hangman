export class WordLetter {
  letter: string;
  found: boolean;

  constructor(letter: string, found = false) {
    this.letter = letter;
    this.found = found;
  }
}
