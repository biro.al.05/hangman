export class SelectableLetter {
  letter: string;
  tried = false;

  constructor(letter: string) {
    this.letter = letter;
  }
}
