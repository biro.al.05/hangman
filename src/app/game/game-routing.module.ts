import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {GameComponent} from './components/game/game.component';
import {GameSettingsComponent} from './components/game-settings/game-settings.component';

const gameRoutes: Routes = [
  {
    path: 'game-settings',
    component: GameSettingsComponent
  },
  {
    path: '',
    component: GameComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(gameRoutes)],
  exports: [RouterModule]
})
export class GameRoutingModule {
}
