import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './components/game/game.component';
import {GameRoutingModule} from './game-routing.module';
import { GameSettingsComponent } from './components/game-settings/game-settings.component';
import { LetterSelectorComponent } from './components/game/letter-selector/letter-selector.component';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [GameComponent, GameSettingsComponent, LetterSelectorComponent],
  imports: [
    CommonModule,
    GameRoutingModule,
    SharedModule
  ]
})
export class GameModule { }
