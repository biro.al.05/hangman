import {RouterModule, Routes} from '@angular/router';
import {LandingComponent} from './landing.component';
import {NgModule} from '@angular/core';

const landingRoutes: Routes = [
  {
    path: '',
    component: LandingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(landingRoutes)],
  exports: [RouterModule]
})
export class LandingRoutingModule {
}
