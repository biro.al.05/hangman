import {Component, OnInit} from '@angular/core';
import {HangmanService} from './shared/hangman.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'hangman';

  constructor(private hangmanService: HangmanService) {
  }

  ngOnInit() {
    this.initWords();
  }

  initWords(): void {
    this.hangmanService.getWordList().subscribe(words => {
      this.hangmanService.wordList.next(words);
      this.hangmanService.calculateWordLengths();
    });
  }
}
