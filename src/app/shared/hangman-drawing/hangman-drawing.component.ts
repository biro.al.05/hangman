import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-hangman-drawing',
  templateUrl: './hangman-drawing.component.html',
  styleUrls: ['./hangman-drawing.component.scss']
})
export class HangmanDrawingComponent {
  @Input() score = 0;
  @Input() displayFull = false;

}
