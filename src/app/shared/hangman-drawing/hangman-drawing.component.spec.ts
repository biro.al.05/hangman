import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HangmanDrawingComponent } from './hangman-drawing.component';

describe('HangmanDrawingComponent', () => {
  let component: HangmanDrawingComponent;
  let fixture: ComponentFixture<HangmanDrawingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HangmanDrawingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HangmanDrawingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
