import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'aAn'
})
export class AAnPipe implements PipeTransform {

    withAn = [8, 11];

    transform(value: number): string {
        if (value) {
          return value === 8 || value === 11 ?
            `an ${value}` :
            `a ${value}`;
        } else {
            return '';
        }
    }
}
