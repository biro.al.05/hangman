import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {BehaviorSubject, Observable} from 'rxjs';


@Injectable({providedIn: 'root'})
export class HangmanService {

  wordList = new BehaviorSubject<Array<string>>(null);
  wordLengths = new BehaviorSubject<Array<number>>(null);
  selectedLength = 0;


  constructor(private http: HttpClient) {
  }

  getWordList(): Observable<Array<string>> {
    const url = `/assets/files/hangman_words.json`;
    return this.http.get(url).pipe(map(res => res as Array<string>));
  }

  calculateWordLengths(): void {
    const lengths = Array.from(
      new Set(this.wordList.value.map(word => word.length))
    );
    lengths.sort((n1, n2) => n1 - n2);
    this.wordLengths.next(lengths);
  }

  getRandomWord(): string {
    const wordlist = this.selectedLength ?
      this.wordList.value.filter(word => word.length === this.selectedLength) :
      this.wordList.value;
    const randomIndex = Math.floor(Math.random() * (wordlist.length - 1));
    return wordlist[randomIndex];
  }
}

