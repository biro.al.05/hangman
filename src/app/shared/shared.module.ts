import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HangmanDrawingComponent } from './hangman-drawing/hangman-drawing.component';
import {AAnPipe} from './pipes/a-an.pipe';



@NgModule({
  declarations: [HangmanDrawingComponent, AAnPipe],
  imports: [
    CommonModule
  ],
  exports: [
    HangmanDrawingComponent, AAnPipe
  ]
})
export class SharedModule { }
